import 'package:flutter/material.dart';

void main() {}

abstract class IDogBreed {
  String? name;
  int? minWeight;
  int? maxWeight;
}

class Stuff implements IDogBreed {
  @override
  int? maxWeight = 60;

  @override
  int? minWeight = 15;

  @override
  String? name = 'Stuff';
}

class Dachshund implements IDogBreed {
  @override
  int? maxWeight = 60;

  @override
  int? minWeight = 15;

  @override
  String? name = 'Stuff';
}

class Request {
  Map<int, String> list = {1: 'Loading', 2: 'Data received'};
  String error = 'Error';
}
class Query extends Request {

}
